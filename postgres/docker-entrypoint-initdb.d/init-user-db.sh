#!/bin/bash
set -e

psql ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER ebis_db_maker WITH SUPERUSER PASSWORD 'arigato36';
    CREATE USER ebisnejp2_log_admin WITH CREATEDB PASSWORD 'wwfpzkXL';
    CREATE DATABASE ebisnejp2_log OWNER ebisnejp2_log_admin;
    CREATE DATABASE ebis;
    CREATE DATABASE id_ebis;
    CREATE DATABASE cs_ebis;
    CREATE DATABASE autobid_ebis;
EOSQL

#import data
psql -U postgres ebisnejp2_log < /data/ebisnejp2_log.dump
psql -U postgres ebis < /data/ebisnejp2_log.dump
psql -U postgres id_ebis < /data/ebisnejp2_log.dump
psql -U postgres cs_ebis < /data/ebisnejp2_log.dump
psql -U postgres autobid_ebis < /data/autobid_ebis.dump