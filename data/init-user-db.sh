#!/bin/bash
set -e

psql ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER ebis_db_maker WITH SUPERUSER PASSWORD 'arigato36';
    CREATE USER ebisnejp2_log_admin WITH CREATEDB PASSWORD 'wwfpzkXL';
    CREATE DATABASE ebisnejp2_log OWNER ebisnejp2_log_admin;
    CREATE DATABASE ebis;
    CREATE DATABASE id_ebis;
    CREATE DATABASE cs_ebis;
    CREATE DATABASE autobid_ebis;
EOSQL

#TODO: only data ebisnejp2
#Issues
psql -U postgres ebisnejp2_log < /docker-entrypoint-initdb.d/schema/ebisnejp2_log.schema.sql
psql -U postgres ebis < /docker-entrypoint-initdb.d/schema/ebis.schema.sql
psql -U postgres id_ebis < /docker-entrypoint-initdb.d/schema/id_ebis.schema.sql
psql -U postgres cs_ebis < /docker-entrypoint-initdb.d/schema/cs_ebis.schema.sql
#psql -U postgres autobid_ebis < /docker-entrypoint-initdb.d/schema/autobid_ebis.schema.sql
#restore data
psql -U postgres ebisnejp2_log < /docker-entrypoint-initdb.d/data/ebisnejp2_log.lite.data.sql
psql -U postgres ebis < /docker-entrypoint-initdb.d/data/ebis.lite.data.sql
psql -U postgres id_ebis < /docker-entrypoint-initdb.d/data/id_ebis.lite.data.sql
#psql -U postgres cs_ebis < /docker-entrypoint-initdb.d/data/cs_ebis.data.sql
#psql -U postgres autobid_ebis < /docker-entrypoint-initdb.d/data/autobid_ebis.data.sql
#local config: db_ip_refer, db_ip_get
psql -U postgres ebis -c "UPDATE config SET db_ip_refer='postgres_server', db_ip_get='mysql_server'"
psql -U postgres ebis -c "UPDATE mtb_host SET ip='postgres_server'"
psql -U postgres ebis -c "UPDATE mtb_host SET delete_flag = 't' WHERE type = 'seo_db' AND delete_flag = 'f'"
#generate conn_XXX.php
## 1.Update id_ebis.mtb_services.redirect_uri
psql -U postgres id_ebis -c "UPDATE mtb_services SET redirect_uri='https://local.hotei.ebis.ne.jp/home.php' WHERE id=1"

# Minimum data Use for listing linking function
psql -U postgres id_ebis -c "DELETE FROM mtb_services WHERE id=2 AND service_key='autobid'"
psql -U postgres id_ebis -c "INSERT INTO mtb_services (id,service_name,service_key,service_secret,redirect_uri,create_date,update_date) VALUES 
(2,'Autobid','autobid','dshj4u8LpewhIeqq','https://local.ab.ebis.ne.jp:8443/dashboard/','2012-07-04 09:57:24.188','2012-07-04 09:57:24.188')"

psql -U postgres id_ebis -c "DELETE FROM mtb_account_links WHERE id=5"
psql -U postgres id_ebis -c "INSERT INTO mtb_account_links (id,create_date,update_date,delete_date) VALUES (5,'2012-07-07 13:53:06.233','2012-07-07 13:53:06.233',NULL)"

psql -U postgres id_ebis -c "DELETE FROM mtb_accounts WHERE service_id=2 and account_no=5013 and account_key like 'ebisnejp2' and account_link_id=5"
psql -U postgres id_ebis -c "INSERT INTO mtb_accounts (service_id,account_no,account_key,account_link_id,create_date,update_date,delete_date) VALUES (2,5013,'ebisnejp2',5,'2012-07-07 09:54:45.510','2012-07-07 09:54:45.510',NULL)"

psql -U ebisnejp2_log_admin ebisnejp2_log -c "DELETE FROM config WHERE id in (1)"
psql -U ebisnejp2_log_admin ebisnejp2_log -c "INSERT INTO config (id,expiredays,ad_expire_time,session_expire,host_name,reject_host,alert_email,conv_duplication_flag,log_max,conversion_max,ad_max,contract_type,day_batch_lastrun,min_batch_lastrun,log_cnt,page_rows_num,page_rows,ad_conv_flag,redirect_ad_flag,redirect_ad_url,regist_page_max,lpo_flag,ad_group1,ad_group2,ad_note,other1,other2,other3,other4,other5,ad_general,login_ip,ad_update_date,custom_start_date,custom_pid,custom_query,custom_type,custom_report_email,agent_option_flag,regist_auto_page_max,directory_index,media_report_batch_lastrun,onetag_max,vt_analyze_batch_lastrun,onetag_detail_max,onetag_detail_rule_max,onetag_rule_max,onetag_rule_pattern_max,lpo_regist_page_max,conversion_type_max,rcv_model_option,itp_flag,cross_domain_flag,cross_domain_list) VALUES 
(1,366,31622400,30,'https://www.ebis.ne.jp/,https://f.msgs.jp/,https://www.lockon.co.jp/,https://www.ec-cube.net/,https://www.mm-lab.jp/,https://three.ne.jp/,http://my.ebis.ne.jp,https://blog.ebis.ne.jp,https://pages.ebis.ne.jp/,https://event.ebis.ne.jp/','118.69.182.220,122.218.226.242,210.138.216.232,221.241.133.218,221.250.87.66','','t',12000000,30,1500000,'12345678','2019-09-09 07:41:44.000','2019-08-12 12:25:00.000',366745,10,100,'t','f',NULL,20000,'t','ad group 01','ad group 02','備考','O1','O2','O3','O4','O5',1,NULL,'2019-06-28 10:33:26.477',NULL,NULL,NULL,NULL,NULL,'t',80000,'index.htm,index.html,index.php','2018-12-31 08:02:10.520',50,'2019-08-12 02:41:01.689',100,5,100,5,10,'{"2":10,"3":10}','1','t','t','ebis.ne.jp|msgs.jp|lockon.co.jp|ec-cube.net|mm-lab.jp|three.ne.jp')"

psql -U ebisnejp2_log_admin ebisnejp2_log -c "DELETE FROM mtb_media WHERE media_id in (1, 2, 3)"
psql -U ebisnejp2_log_admin ebisnejp2_log -c "INSERT INTO mtb_media (media_id,media_name,media_caption,rank,delete_flag) VALUES (1,'Google','Google',1,'f'),(2,'YSS','YSS',2,'f'),(3,'YDN','YDN',3,'f')"
