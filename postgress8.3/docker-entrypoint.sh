#!/bin/bash
set +e

POSTGRES_USER="postgres"
POSTGRES_PASSWORD="'arigato36'"

su - postgres -c "
export PATH=$PATH:/usr/local/pgsql/bin &&\
echo \"export PATH=$PATH:/usr/local/pgsql/bin\" >> /home/postgres/.bashrc &&\
pg_ctl -w -D /usr/local/pgsql/data -l /usr/local/pgsql/data/logfile stop &&\
if psql -t -c '\du' | cut -d \| -f 1 | grep -qw \"$POSTGRES_USER\"; then echo \"main db user already exists\"; else psql -h 127.0.0.1 --command \"CREATE USER $POSTGRES_USER WITH SUPERUSER PASSWORD $POSTGRES_PASSWORD;\"; fi
"

su - postgres -c "
export PATH=$PATH:/usr/local/pgsql/bin &&\
echo \"export PATH=$PATH:/usr/local/pgsql/bin\" >> /home/postgres/.bashrc &&\
pg_ctl -w -D /usr/local/pgsql/data -l /usr/local/pgsql/data/logfile start &&\
if psql -t -c '\du' | cut -d \| -f 1 | grep -qw \"$POSTGRES_USER\"; then echo \"main db user already exists\"; else psql -h 127.0.0.1 --command \"CREATE USER $POSTGRES_USER WITH SUPERUSER PASSWORD $POSTGRES_PASSWORD;\"; fi
"

for f in /docker-entrypoint-initdb.d/*; do
	case "$f" in
		*.sh)
			# https://github.com/docker-library/postgres/issues/450#issuecomment-393167936
			# https://github.com/docker-library/postgres/pull/452
			if [ -x "$f" ]; then
				echo "$0: running $f"
				"$f"
			else
				echo "$0: sourcing $f"
				. "$f"
			fi
			;;
		*.sql)    echo "$0: running $f"; "${psql[@]}" -f "$f"; echo ;;
		*.sql.gz) echo "$0: running $f"; gunzip -c "$f" | "${psql[@]}"; echo ;;
		*)        echo "$0: ignoring $f" ;;
	esac
	echo
done

exec "$@";
