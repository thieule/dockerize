## Quickstart

### 1. Checkout config files from Bitbucket

**Make sure git config `autocrlf=false`**
Verify all file `.sh` using **Unix Line Endings(LF)**

```
git config --global core.autocrlf false
git clone git@bitbucket.org:thieule/dockerize.git
```

### 2. Modify config

**2.2. Create `.env`**

```
cp .env-dev .env
```

correct `WORKSPACE_PATH_HOST`
Modify others env variable if needed

```
cp docker-compose-dev.yml docker-compose.yml
```

## 3. Start services

```
#start all or update config or docker-compose.yml
docker-compose up -d

#stop all
docker-compose stop

#check container running
docker-compose ps

#Run a command in a running container `postgres_server`
docker-compose exec postgres_server bash
```