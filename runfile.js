const { run, help, options } = require('runjs');
 
function clean() {
    run("docker-compose down");
    run("docker system prune --all --force");
    run("docker volume prune --force");
}
help(clean, "Clean all docker in PC");

function build(env) {
    var opts = options(this);

    // Clean all images
    if(opts.clean || opts.c) {
        clean();
    }

    // Update environment variable if process run on windows
    if(process.platform === "win32") {
        process.env.COMPOSE_CONVERT_WINDOWS_PATHS=1;
    }

    if(env !== undefined) {
        run("docker-compose -f docker-compose-" + env +  ".yml up -d");
    } else {
        // Local
        run("docker-compose build");
        run("docker-compose up -d");
    }
}

help(build, {
    description: `Build docker images on specific
     env docker compose(docker-compose-<<env>>.yml)`,
    params: ['env'],
    options: {
      "c": 'clean images first',
      "clean": 'clean images first'
    },
    examples: `
      run build
      run build --clean
      run build --clean dev
      run build -c test
    `
  });

module.exports = {
    clean,
    build
};