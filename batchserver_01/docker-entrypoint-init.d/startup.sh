#!/bin/bash
set -e

if [ ! -d /var/log/ebis/ ]; then
    mkdir -p /var/log/ebis/
    chmod 777 -R /var/log/ebis/
fi

if [ ! -L /home/hoteiebisadmin/ebis_custom_report ]; then
    ln -s /mnt/workspace/ebis_custom_report /home/hoteiebisadmin/ebis_custom_report
    ln -s /mnt/workspace/ebis_acbatch /home/hoteiebisadmin/ebis_acbatch
fi

# set host in hosts
line=$(head -n 1 /etc/hosts)
line2=$(echo $line | awk '{print $2}')
hostname=$(hostname)
echo "$line $line2.localdomain $hostname" >> /etc/hosts

#remove localhost limit
sed -i -e "s/Port=smtp,Addr=127.0.0.1, Name=MTA/Port=smtp, Name=MTA/g" \
    /etc/mail/sendmail.mc
sendmail -bd